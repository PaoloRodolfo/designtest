﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lives : MonoBehaviour

{
    public int playerLives = 2;
    public bool isDead = false;

    public int GetPlayerLives()
    {
        return playerLives;
    }

    public void SetPlayerLives(int value)
    {
        playerLives = value;
    }


    void Update()
    {

        if (isDead == true && playerLives >= 1)
        {
            playerLives--;
            //Instantiate(playerPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 180));
            transform.position = new Vector3(0, 1, 0);
            //player = GameObject.FindGameObjectWithTag("Player");
            isDead = false;
        }

        else if (isDead==true)
        {
            Time.timeScale = 0;
        }
    }
}