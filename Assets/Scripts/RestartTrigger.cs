﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartTrigger : MonoBehaviour
{

    private Scene scene;
    
    void Start()
    {
        scene = SceneManager.GetActiveScene();
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Application.LoadLevel(scene.name);
            //Destroy(other.gameObject);
            other.gameObject.GetComponent<Lives>().isDead = true;
        }
        else
        { 
         
        }
    }
}
