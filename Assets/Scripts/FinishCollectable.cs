﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishCollectable : MonoBehaviour
{

    public GameObject completeLevelUI;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Time.timeScale = 0;
            completeLevelUI.SetActive(true);
        }
    }
}
